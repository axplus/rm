// 2017-8-24

#import <Foundation/Foundation.h>
#include <getopt.h>

void printhelp() {
  fprintf(stderr, "rm v1.0 by yijiezeng, remvoe file or directory into trash.\n");
}

int main(int argc, char **argv) {
  @autoreleasepool {
    int c = 0;
    int force = 0;
    int rec = 0;
    int dir = 0;
    int verbose = 0;
    while ((c = getopt(argc, argv, "hvrdf")) != EOF) {
      switch (c) {
      case 'h':
        printhelp();
        return 1;
      case 'v':
        verbose = 1;
        break;
      case 'r':
        rec = 1;
        break;
      case 'd':
        dir = 1;
        break;
      case 'f':
        force = 1;
        break;
      }
    }

    NSFileManager *mgr = [NSFileManager defaultManager];
    NSError *e = nil;

    for (int i = optind; i < argc; i++) {
      const char *path = argv[i];
      NSString *spath = [NSString stringWithUTF8String:path];
      NSURL *fileurl = [NSURL fileURLWithPath:spath isDirectory:NO];
      if (verbose) {
        NSLog(@"spath=%@ fileurl=%@", spath, fileurl); //////////
      }

      // todo:check dir

      if (![mgr trashItemAtURL:fileurl resultingItemURL:nil error:&e]) {
        if (force) {
          continue;
        }
        NSLog(@"error=%@", e); ////////////
        return e.code;
      }
    }
    return 0;
  }
}
